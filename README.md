# Introduction
Jersey STEM currently runs a multi-instance, linux based, headless MinecraftEDU server.  Some notable differences from running Minecraft on your desktop:
- MinecraftEDU (MCE) is NOT Minecraft.  It has a bunch of added 'features', and is fundamentally different.  Location of config files is different, plugins are proprietary to MCE.  Its best to not assume that MCE is administered similarly to Minecraft.  Gameplay has a very similar environment and experience, but the administration is fairly unique.
- A 'headless' server is a computer that does not have a monitor.  Typically these are things that are used remotely.  You probably use headless servers today without realizing it.  Email and Web servers are headless.  Just because there is no monitor does not mean there is no user interface.  You can access the server remotely via secure shell (SSH), web browser, or a graphical interface via X11, VNC, or RDP.
- The server has a lot of power, so it is able to run many multiple instances of MCE.  Currently we are running eight concurrent instances of MCE, each with its own world.

This document will continuously change as the server is worked on, and we'll do our best to ensure it is kept up to date.  Please check back regularly.  The intent and purpose is to keep details of how the server is configured and how to administer it, so that it can be continuously supported by current admins as well as their future replacements.  Abiding by the goal to continuously have two people in training to replace a particular function/role within JerseySTEM, it is important to document and minimize leaning on 'tribal' information.

# Administration
## Secure Shell
### Connection
If you are unfamiliar with SSH, I recommend starting with following a tutorial @ http://support.suso.com/supki/SSH_Tutorial_for_Windows
Settings for connecting to the server:
- Hostname: minecraft.jerseystem.net
- Username: minecraft
- Password: emailed monthly to minecraft coaches distribution list

### MCE Admin Commands

| Command	|	 Description  |
|-----------|-----------------|
| *aliases-show*	| Show current aliases allowed to login to game |
|*aliases-download* | Download aliases and enforce curfew |
|*server-status*	| List each of the servers, their status, and associated world |
|*world-dl*	| Download a new world from http://services.minecraftedu.com/worlds into ~/worlds.  only argument is the URL of the map.  |
|*world-link*	| Associates a world in ~/worlds to a particular server. First  argument is the world directory name, second argument is the  server name. |
|*world-reset*	| Unpacks (and overwrites) a particular world in ~/worlds.  First and only argument is the world directory name.|

#### Mods
Read more here: http://services.minecraftedu.com/wiki/Taking_it_Further#Using_MinecraftEdu_Online_Mods_Repository_to_download_new_mods
Steps to procuring and making a new mod available (on all instances)
1. Get mod: 
   - http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods
   - http://modlist.mcf.li/version
2. Put mod where it needs to go (do not unzip):
   - If it is a client or both mod, put into a client installer and distribute to all students, goes into minecraftedu\minecraft\mods\1.7.10
   - If it is a server or both mod, put into server path in ~/serverX/minecraftedu\servertool\server\mods\1.7.10 using the `mod-link` command:
        - `mod-link worldedit-forge-mc1.7.10-6.0-beta-01.jar` 

# Future Plans
Stuff we're planning to do will go here.

### Minecraft EDU Web Admin
A web based admin interface to the Jersey STEM minecraft Edu server. 

#### Web Structure
 - Index - Overview of options
 - Profile - Edit my own user account
 - User Admin (create/delete admin users)
 - Player Management (may or may not do this, to replace existing web form)
 - Server (instance) Status
   - Edit Server
     - 'Lock/Unlock' instance (enable/disable student access)
     - Link world
     - Server (instance) Status 
       - online/offline, port, current world, # active players
       - student lock/open
       - Login whitelist (readonly)
         - Force update/download
       - Stop/Start/Restart instance
  - Mod Management
    - List current Mods
    - Enable/Disable mod
    - Install new mod
    - Download all active mods
  - World Management
     - Install new world
     - Restore World
     - Clone World
  - Online Help (contextual and throughout)


